$(function() {

    chrome.storage.sync.get(['key'], function(result) {
        console.log('Value currently is ' + result.key);
        if(result.key == 'play') {
            $('.playButton').hide();
            $('.stopButton').show('slow');
            $('img').removeClass('App-logo-static').addClass('App-logo');
        } else if(result.key == 'stop') {
            $('.stopButton').hide();
            $('.playButton').fadeIn('slow');
            $('img').removeClass('App-logo').addClass('App-logo-static');
        }
    });

    function notifID() {
        var id = Math.floor(Math.random() * 9007199254740992) + 1;
        return id.toString();
    }
    
    $('.playButton').on('click', function () {
        $(this).hide();
        $('.stopButton').fadeIn('slow');
        $('img').removeClass('App-logo-static').addClass('App-logo');
        chrome.extension.sendMessage({action: "play"}, function(response) {
            if(response === "internetError") {
                var notificationOptions = {
                    type: 'basic',
                    iconUrl: 'img/icon128.png',
                    title: 'Ifastek Radio',
                    message: 'A network error occurred, we were unable to stream your radio. Please check your internet connection.'
                };
                chrome.notifications.create(notifID(), notificationOptions, function() {});

                $('.stopButton').hide();
                $('.playButton').fadeIn('slow');
                $('img').removeClass('App-logo').addClass('App-logo-static');
            }
        });

        chrome.storage.sync.set({key: 'play'}, function(value) {
            console.log('Value is set to ' + value);
        });
        
    });

    $('.stopButton').on('click', function () {
        $(this).hide();
        $('.playButton').fadeIn('slow');
        $('img').removeClass('App-logo').addClass('App-logo-static');
        chrome.extension.sendMessage({action: "stop"});

        chrome.storage.sync.set({key: 'stop'}, function(value) {
            console.log('Value is set to ' + value);
        });
    });


})